package ua.sergiishapoval.globallogic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Сергей on 14.01.2015.
 */
public class HighestArraySum {

    public static void main(String[] args) {

        int[][] lists = new int[][]{
                {5,4,3,2,1}, 
                {4,1}, 
                {5,0,0}, 
                {6,4,2}, 
                {1} 
        };

        System.out.println(findHighestSums(lists, -1));
        System.out.println(findHighestSums(lists, 3));
        System.out.println(findHighestSums(lists, 5));
        System.out.println(findHighestSums(lists, 50));
        System.out.println(findHighestSums(lists, 450));
    }

    public static List<Integer> findHighestSums(int[][] lists, int n) {

        List<Integer> resultList = new ArrayList<>();
        if (n<1) return resultList;
        else {
/*Deep copy of given array start*/
            int [][] listsBuffer = new int[lists.length][1];
            for (int i = 0; i < lists.length; i++) {
                listsBuffer[i] = lists[i].clone();
            }
/*Deep copy of given array end*/

/*calculate maxSum and convert values in difference with max in current array - start*/
            int maxSum = 0;
            for (int[] list : listsBuffer) {
                maxSum += list[0];
    //all values in an array are decreased by first value
                for (int j = 1; j < list.length; j++) {
                    list[j] -= list[0];
                }
    //first value will be changed last
                list[0] = 0;
            }
/*calculate maxSum and convert values in difference with max in current array - end*/

/*find max index to stop not necessary steps - start*/
            List<ArrayElement> elementList = new ArrayList<>();
    /*work only with elements with 1+ index*/
            for (int i = 0; i < listsBuffer.length; i++) {
                for (int j = 1; j < n && j < listsBuffer[i].length ; j++) {
                    elementList.add(new ArrayElement(i, listsBuffer[i][j]));
                }
            }
            Collections.sort(elementList);
    //at list one element should be
            int maxIndexArray[] = new int[listsBuffer.length];
            for(int i = 0; i < listsBuffer.length; i++){
                maxIndexArray[i] = 1;
            }
    //we need only n sums, so i < n -1
            for(int i = 0; i < n - 1 && i < elementList.size(); i++){
                maxIndexArray[elementList.get(i).getArrayNum()] ++;
            }
/*find max index to stop not necessary steps - end*/

/*calculate all possible sums within index limits - start*/
            List<Integer> highestValues = new ArrayList<>();
            int genericArray[] = new int[listsBuffer.length];

        /*
        increase index on the first level and
        if current level finished -  increase next one by one,
        check next for limits and repeat,
        finished when nothing possible to increase
        */

           breakPoint:
            for(int i = 0;;){
                int sum = 0;
                for(int j = 0; j < listsBuffer.length; j++){
                    sum += listsBuffer[j][genericArray[j]];
                }
                highestValues.add(sum + maxSum);

                genericArray[i] ++;
                if(genericArray[i] >= maxIndexArray[i]){
                    genericArray[i] = 0;
                    i++;
                    genericArray[i]++;
                    while(genericArray[i] >= maxIndexArray[i]){
                        genericArray[i] = 0;
                        i++;
                        if(i >= listsBuffer.length){
                            break breakPoint;
                        }
                        genericArray[i]++;
                    }
                    i = 0;
                }
            }
/*calculate all possible sums within index limits - end*/

            Collections.sort(highestValues);
//sort is done from min to max, we need reverse order, consider combination limit
            for (int i = 0; i < n && i < highestValues.size(); i++) {
                resultList.add(highestValues.get(highestValues.size() - i - 1));
            }

            return resultList;
        }
    }
}
