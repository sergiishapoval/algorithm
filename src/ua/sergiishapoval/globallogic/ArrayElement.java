package ua.sergiishapoval.globallogic;

/**
 * Created by Сергей on 14.01.2015.
 */
public class ArrayElement implements Comparable<ArrayElement> {

    private int arrayNum;
    private int value;

    public ArrayElement(int arrayNum, int value) {
        this.arrayNum = arrayNum;
        this.value = value;
    }

    public int getArrayNum() {
        return arrayNum;
    }

    public int getValue() {
        return value;
    }

    @Override
    public int compareTo(ArrayElement o) {
        return o.value - value;
    }

    @Override
    public String toString() {
        return "{arrayNum=" + arrayNum +
                ", value=" + value +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArrayElement)) return false;

        ArrayElement that = (ArrayElement) o;

        if (arrayNum != that.arrayNum) return false;
        if (value != that.value) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = arrayNum;
        result = 31 * result + value;
        return result;
    }
}
